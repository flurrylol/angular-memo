# Angular Memo

* [Custom Structural Directives](#custom-structural-directive)
* [ngSwitch](#ng-switch-)
* [HostBinding et HostListener](#hostbinding-hostlistener)

## Custom structural directive :

```@Directive({
  selector: '[directiveName]'
})

@Input() set directiveName(condition: bolean) {
  if (!condition){
    this.vcRef.createEmbeddedView(this.templateRef);
  } else {
     this.vcRef.clear();
  }
}

constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) {}
```

**Warning** : Dans l'appel de la structural directive sur le component, ne pas oublier de bien prendre *directiveName*, avec le selector et l'input qui ont le meme nom.      



## Ng Switch :

```
<div [ngSwitch]="value">
  <p *ngSwitchCase="value === 5">Value is 5</p>
  <p *ngSwitchCase="value === 10">Value is 10 </p>
  <p *ngSwitchDefault>Value is Default</p>
</div>
```


## HostBinding HostListener

Hostbinding | HostListener
----------- | ------------
HostBinding permet d'ajouter une propriété CSS lorsque le booleen est true. | HostListener permet d'effectuer une action lorsque l'évènement est déclenché
`@HostBinding('opencssclass') isOpen = false;` | `@HostListener('click') toggleOpen(){ this.isOpen = !this.isOpen }`
